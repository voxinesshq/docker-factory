robot virtualbox
================

build a virtualbox vm with robot ride and co

pre-requisites
==============
be on machine with virtualbox,vagrant and ansible



jessie vmbox base 
=================


minimal debian-jessie

Debian Jessie amd64 minimal w/ guest additions (Weekly updates)	VirtualBox	 

box: https://downloads.sourceforge.net/project/vagrantboxjessie/debian80.box	

size: 693 Mb


things to install

+
apt-get install xorg
apt-get install lxde
apt-get install iceweasel
apt-get install vim


git-core
+

python python-pip

wxPython 2.8.12.1  with Unicode support
robotframework
ride

+ options
docker
ansible
sublimetext
