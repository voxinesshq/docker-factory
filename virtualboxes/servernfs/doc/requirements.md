Container sous Ansible
albert.maman@orange.com <albert.maman@orange.com>	4 novembre 2014 11:05
À : "tordjman.laurent@gmail.com" <tordjman.laurent@gmail.com>
Cc : RENOUARD Jean Marie Ext IMT/IBNF <jrenouard.ext@orange.com>, HUSS Sebastien Ext IMT/IBNF <shuss.ext@orange.com>
Salut Laurent,

 

En vue de la création d’une future VM (disons SNOW)  pour complèter SURF et KITE.

voici les 20 étapes à jouer après l’installation du Kit Platon Linux :

 

Je souhaiterait avoir une illustration d’un container ANSIBLE pour cette opération

mais uniquement pour les 4 premières étapes, afin d’évaluer la faisabilité :

 

Pourrais-je avoir une présentation pour la réalisation d’un tel container

Sur votre infrastructure dans une premier temps, qui dois-je contacter.

 

 

=======================================================================

PREREQUIS :

         Installation d’une VM avec DVD RH64  CommonBundle

         Choix  lowdisk

         (cela utilise 25 Go pour Linux (15Go rootvg + 9Go infravg)

=======================================================================

 

 

#=================================================================================

# Se connecter root

# puis on commence par configurer le reseau, pour pouvoir poursuivre ensuite sur un ecran Putty

 

#===================

# 1- HOSTNAME

#===================

 

                vi /etc/sysconfig/network

NETWORKING=yes

HOSTNAME=kite

NOZEROCONF=1

NETWORKING_IPV6=no

 

                vi /etc/hosts

10.179.0.15     kite

10.179.0.200    surf

 

Puis taper :

                hostname kite

 

#===================

# 2- ADDDRESS IP

#===================

 

                vi /etc/sysconfig/network-scripts/ifcfg-eth0

DEVICE="eth0"

HWADDR="00:19:DB:42:C3:99"

NM_CONTROLLED="yes"

ONBOOT="yes"

BOOTPROTO=none

NETMASK=255.255.252.0

IPADDR=10.179.0.15

GATEWAY=10.179.3.254

TYPE=Ethernet

 

                service network restart

 

# Reconnexion avec Putty cette fois-ci :

# Cela permet de faire des copier/coller depuis cette trace.

 

#===================

# 3- NAMES SERVER

#===================

 

                vi /etc/resolv.conf

 

domain stquentiny.francetelecom.fr

search stquentiny.francetelecom.fr

nameserver 10.171.108.2

nameserver 10.171.30.4

nameserver 10.171.34.57

  

 

#============================

# 4- SSH permission for root

#============================

 

               vi /etc/ssh/sshd_config   

      (commencer par la fin du fichier car les dernieres lignes prevalent.)

 

PermitRootLogin yes                     # yes au lieu de no

# AllowGroups staff ucmdb acces_ssh     # a dieser

# DenyUsers root                        # a dieser

# DenyGroups root                       # a dieser

# ClientAliveInterval 300               # a dieser  c'est un timeout de 5mn pour ssh scp sftp

 

 

Puis relancer le service :

          service sshd restart

 

#===========================================

# 5- SSH echange des cles entre kite et surf

#===========================================

 

su - root

mkdir -p $HOME/.ssh

chmod 700 $HOME/.ssh

cd $HOME/.ssh

ssh-keygen -t rsa -b 1024 -f $HOME/.ssh/id_rsa -N ""

 

cat id_rsa.pub  >> authorized_keys_on_the_opposit_machine

 

chmod 600 $HOME/.ssh/id_rsa.pub

chmod 600 $HOME/.ssh/authorized_keys

 

 

#=========================================================================================

# 5- PARTITIONNEMENT DU RESTE DU DISQUE :

#    On le fait maintenant, car ensuite la creation des users  was et db2 s'appuie sur /liv

#=========================================================================================

 

Vu que c'est un Disque de 500GB (au lieu de 1 Tera)

on ne va creer que  /liv (420 Go)

                et  /web ( 30 Go)

et on ne recree pas /ref (pas de place)

 

 

#================================

# Infos de vgdisplay     recopiees pour disposer ici de VG-UUID

#================================

VG-name  Taille   Used  Free Type PEsize  VG-UUID

infravg    8.97   2.00  6.97 lvm2 32 MB   YSG6AZ-hPpQ-lCaj-42Ga-REpM-F0jO-AxOLdH

rootvg    14.97  11.62  3.34 lvm2 32 MB   KtSplR-Y1T7-UyML-gSYb-Ry1g-9RPR-XoPQZL

 

#================================

# df -k

#================================

Filesystem                  1K-blocks      Used Available Use% Mounted on

/dev/mapper/rootvg-root_lv    3096336    295328   2643724  11% /

tmpfs                          993840         0    993840   0% /dev/shm

/dev/sda1                      253871     33019    207745  14% /boot

/dev/mapper/rootvg-home_lv    1032088     34136    945524   4% /home

/dev/mapper/rootvg-opt_lv      516040     16796    473032   4% /opt

/dev/mapper/rootvg-system_lv   126931     12927    107451  11% /system

/dev/mapper/rootvg-tmp_lv     1032088     34056    945604   4% /tmp

/dev/mapper/rootvg-usr_lv     4128448    683652   3235084  18% /usr

/dev/mapper/rootvg-var_lv     1032088     66744    912916   7% /var

/dev/mapper/infravg-images_lv 4128448    139256   3779480   4% /images

 

                # infravg (donc /images) est dans /dev/sda3     # vu par fdisk p

                # Les autres LV de rootvg sont dans /dev/sda2   # vu par fdisk p

 

#==============================================================

# Creation d'une partition Etendue

#  pour les 2 futures vrais LV de  /liv et /web

#==============================================================

 

[root@kite ~]# fdisk /dev/sda

 

The device presents a logical sector size that is smaller than

the physical sector size. Aligning to a physical sector (or optimal

I/O) size boundary is recommended, or performance may be impacted.

 

WARNING: DOS-compatible mode is deprecated. It's strongly recommended to

         switch off the mode (command 'c') and change display units to

         sectors (command 'u').

 

Command (m for help): p

 

Disk /dev/sda: 500.1 GB, 500107862016 bytes

255 heads, 63 sectors/track, 60801 cylinders

Units = cylinders of 16065 * 512 = 8225280 bytes

Sector size (logical/physical): 512 bytes / 4096 bytes

I/O size (minimum/optimal): 4096 bytes / 4096 bytes

Disk identifier: 0x0000e438

 

   Device Boot      Start         End      Blocks   Id  System

/dev/sda1   *           1          33      262144   83  Linux

Partition 1 does not end on cylinder boundary.

/dev/sda2              33        1991    15728640   8e  Linux LVM

/dev/sda3            1991        3166     9437184   8e  Linux LVM

 

Command (m for help): n

Command action

   e   extended

   p   primary partition (1-4)

e

Selected partition 4

First cylinder (3166-60801, default 3166): 3166

Last cylinder, +cylinders or +size{K,M,G} (3166-60801, default 60801): 60801

 

Command (m for help): p

 

Disk /dev/sda: 500.1 GB, 500107862016 bytes

255 heads, 63 sectors/track, 60801 cylinders

Units = cylinders of 16065 * 512 = 8225280 bytes

Sector size (logical/physical): 512 bytes / 4096 bytes

I/O size (minimum/optimal): 4096 bytes / 4096 bytes

Disk identifier: 0x0000e438

 

   Device Boot      Start         End      Blocks   Id  System

/dev/sda1   *           1          33      262144   83  Linux

Partition 1 does not end on cylinder boundary.

/dev/sda2              33        1991    15728640   8e  Linux LVM

/dev/sda3            1991        3166     9437184   8e  Linux LVM

/dev/sda4            3166       60801   462955040+   5  Extended                # pour les 2 future LV

 

---------------------------------------------------------------------------------------------------

 

Command (m for help): n

First cylinder (3166-60801, default 3166): 3169

        ###  3169 :  Obtenu par tatonnement,

        ###  sinon, si 3166 ou 3167 ou 3168 alors

        ###  Message : Partition 5 does not start on physical sector boundary.

Last cylinder, +cylinders or +size{K,M,G} (3166-60801, default 60801): +420000M

 

Command (m for help): p

 

Disk /dev/sda: 500.1 GB, 500107862016 bytes

255 heads, 63 sectors/track, 60801 cylinders

Units = cylinders of 16065 * 512 = 8225280 bytes

Sector size (logical/physical): 512 bytes / 4096 bytes

I/O size (minimum/optimal): 4096 bytes / 4096 bytes

Disk identifier: 0x0000e438

 

   Device Boot      Start         End      Blocks   Id  System

/dev/sda1   *           1          33      262144   83  Linux

Partition 1 does not end on cylinder boundary.

/dev/sda2              33        1991    15728640   8e  Linux LVM

/dev/sda3            1991        3166     9437184   8e  Linux LVM

/dev/sda4            3166       60801   462955040+   5  Extended                # 460GB les 2 future LV

/dev/sda5            3169       56712   430084148   83  Linux                   # pour /liv  420 GB

 

--------------------------------------------------------------------------------

 

Command (m for help): n

First cylinder (3166-60801, default 3166): 56713

Last cylinder, +cylinders or +size{K,M,G} (56713-60801, default 60801): 60801

 

Command (m for help): p

 

Disk /dev/sda: 500.1 GB, 500107862016 bytes

255 heads, 63 sectors/track, 60801 cylinders

Units = cylinders of 16065 * 512 = 8225280 bytes

Sector size (logical/physical): 512 bytes / 4096 bytes

I/O size (minimum/optimal): 4096 bytes / 4096 bytes

Disk identifier: 0x0000e438

 

   Device Boot      Start         End      Blocks   Id  System

/dev/sda1   *           1          33      262144   83  Linux

Partition 1 does not end on cylinder boundary.

/dev/sda2              33        1991    15728640   8e  Linux LVM

/dev/sda3            1991        3166     9437184   8e  Linux LVM

/dev/sda4            3166       60801   462955040+   5  Extended

/dev/sda5            3169       56712   430084148   83  Linux                   # pour /liv  420 GB

/dev/sda6           56713       60801    32844892+  83  Linux                   # pour /web   30 GB

 

--------------------------------------------------------------------------------

 

Command (m for help): w

The partition table has been altered!

 

Calling ioctl() to re-read partition table.

 

WARNING: Re-reading the partition table failed with error 16: Device or resource busy.

The kernel still uses the old table. The new table will be used at

the next reboot or after you run partprobe(8) or kpartx(8)

Syncing disks.

 

 

 

#===================

# 6- REBOOT

#===================

 

# Ce reboot est necessaire pour voir apparaitre les nouveau /dev/sda5  /dev/sda6 ...

# ( peut-etre a cause du WARNING ci-dessus)

 

reboot

 

        # Aucun message d'anomalie

 

#======================================

# 7- Creation des 2 nouveaux FS et montage

#======================================

 

mkfs.ext3 /dev/sda5            # 420 Go

mkfs.ext3 /dev/sda6            #  30 Go

 

# Ajout des FS dans 
      vi /etc/fstab

/dev/sda5         /liv            ext3    defaults        1 2

/dev/sda6         /web            ext3    defaults        1 2

 

mkdir /liv /web

mount -a

 

#======================================

# 8- Install de  ddff  sous /usr/local/bin

#======================================

 

scp surf:/usr/local/bin/ddff /usr/local/bin

 

 

[root@kite bin]# ddff

______________________________________________________________________________________

 

Filesystem           1024-blocks        Used   Available   Pct_used  Mounted_on

______________________________________________________________________________________

 

/rootvg-root_lv          3096336      289056     2649996        10%  /

tmpfs                     993840           0      993840         0%  /dev/shm

/dev/sda1                 253871       33019      207745        14%  /boot

/rootvg-home_lv          1032088       34140      945520         4%  /home

/infravg-images_lv       4128448      139256     3779480         4%  /images

/rootvg-opt_lv            516040       16796      473032         4%  /opt

/rootvg-system_lv         126931       12927      107451        11%  /system

/rootvg-tmp_lv           1032088       34056      945604         4%  /tmp

/rootvg-usr_lv           4128448      682948     3235788        18%  /usr

/rootvg-var_lv           1032088       65920      913740         7%  /var

/dev/sda5              423334412      203084   401627124         1%  /liv

/dev/sda6               32328692      180284    30506164         1%  /web

______________________________________________________________________________________

 

 

 

 

#===================

# 9- USERS

#===================

 

   groupadd -g 3001 invite

   useradd -u 3001 -g 3001 -d /home/maman  -m -s /usr/bin/ksh maman

   useradd -u 3099 -g 3001 -d /home/invite -m -s /usr/bin/ksh invite

   useradd -u 3097 -g 3001 -d /liv/WAS     -m -s /usr/bin/ksh was

   useradd -u 3096 -g 3001 -d /liv/DB2     -m -s /usr/bin/ksh db2

 

 

#===================

# 10- PASSWORDS

#===================

 

tail /etc/passwd

 

osadmin:x:249:249:Administrateur Platon:/home/osadmin:/bin/bash

ucmdb:x:15000:15000:Utilisateur pour la collecte ucmdb:/home/ucmdb:/bin/bash

maman:x:3001:3001::/home/maman:/usr/bin/ksh

invite:x:3099:3001::/home/invite:/usr/bin/ksh

was:x:3097:3001::/liv/WAS:/usr/bin/ksh

db2:x:3096:3001::/liv/DB2:/usr/bin/ksh

 

 

passwd root     # ...

passwd maman    # ...

passwd invite   # Saisir invite

 

[root@kite ~]# ls -lrt /home

total 32

drwx------. 2 root    root    16384 Dec  5 09:40 lost+found

drwx------. 2 osadmin osadmin  4096 Dec  5 09:48 osadmin

drwx------  3 ucmdb   ucmdb    4096 Dec  5 09:48 ucmdb

drwx------  2 maman   invite   4096 Dec  5 11:33 maman

drwx------  2 invite  invite   4096 Dec  5 11:33 invite

 

        # Le "point" apres les droits concerne les Contexts de Securite de SE Linux

        # Seul certains repertoires initiaux du kit PLATON ont parfois ce "point".

 

 

#===================

# 11- NFS

#===================

 

[root@kite ~]# service nfs start

Starting NFS services:                                     [  OK  ]

Starting NFS mountd:                                       [FAILED]

Starting NFS daemon: rpc.nfsd: writing fd to kernel failed: errno 111 (Connection refused)

rpc.nfsd: unable to set any sockets for nfsd

                                                           [FAILED]

#===================

# 12- ROOT  .profile

#===================


         vi /root/.bash_profile

              On passe TMOUT=0 au lieu de 600

              Et on ajoute ceci :

                   alias tepo="netstat -tepo | grep -v keepalive"

 

#====================

# 13- Server NTP

#====================

 

         Ajout de la ligne suivante dans le crontab de root :

         (Autre possibilite mais non-retenue : Parametrer /etc/ntp.conf

                                               et lancer le daemon : service ntpd start )
         crontab -e

#--------------------------------------------------------------------------------

# Sync date with NTP server every 15 minutes :

#--------------------------------------------------------------------------------

0,15,30,45 *     *     *      * /usr/sbin/ntpdate -b -s ntp-g5-1.si.francetelecom.fr

 

 

 

#========================================

# 14- YUM (repository en local)

#========================================

 

On a recopie tous les rpm RH64 sous /web/RPM_RH64_FROM_DVD_PLATON

mkdir -p /web/RPM_RH64_FROM_DVD_PLATON/Packages

 

Inserer le DVD Platon RH64

mount /dev/cdrom

nohup cp -p  /media/cdrom/Server/Packages/* /web/RPM_RH64_FROM_DVD_PLATON/Packages &

                #  3567 fichiers rpm

nohup cp -rp /media/cdrom/Server/repodata   /web/RPM_RH64_FROM_DVD_PLATON &

 

 

On declare cette nouvelle repository a yum :

vi /etc/yum.repos.d/common-local.repo           # New file

 

        [local-web]

        name=Platon Linux RHEL 6.1

        baseurl=file:///web/RPM_RH64_FROM_DVD_PLATON/

        enabled=1

 

#========================================

# 15- SAMBA

#========================================

yum --enablerepo=local-web install samba-common

yum --enablerepo=local-web install samba

 

Dans /etc/samba/smb.conf ajout du paragraphe [maman] et [invite]

[maman]

        comment = Repertoire personnel

        path = /home/maman

        writable = yes

        valid users = maman

[invite]

        comment = Repertoire des invites

        path = /home/invite

        writeable = no

        valid users = invite

 

 

smbpasswd -a maman      # saisir le password papa

service smb start

chkconfig --level 345 smb     on

 

 

#========================================                   PAS FAIT

# 16- Install du Display Manager GNOME  (GDM)               PAS FAIT

#========================================                   PAS FAIT

#

# yum grouplist

# Cela affiche les groupes

# On y distingue les 2 groupes necessaires :  "Desktop"  "X Window System"

#

# yum groupinstall  "Desktop"           # (251 Packages / 122 Mo)

#

# yum groupinstall  "X Window System"   # ( 91 Packages /  18 Mo)

#========================================               FIN DU PAS FAIT

 

 

#===================

# 17- SFTP

#===================

 

Par defaut : le service sftp est actif. Il est gere par le daemon sshd.

donc pas besoin d'installer un autre serveur ftp :

 

Les processus n'apparaissent que s'il y a une connexion sftp en cours :

[root@localhost home]# ps -ef | grep ftp

invite    2252  2251  0 19:16 ?        00:00:00 ksh -c /usr/libexec/openssh/sftp-server

invite    2253  2252  0 19:16 ?        00:00:00 /usr/libexec/openssh/sftp-server

 

Donc pas besoin d'installer le service vsftp.

 

#===================

# 18- NFS Server

#===================

 

chkconfig rpcbind on    # Eventuellement automatiser au reboot

 

service rpcbind start   # pour tester ponctuellement

 

vi /etc/hosts.allow

Ajouter la ligne  :   rpcbind:ALL               # La ligne etait deja presente

 

[root@kite ~]#  service nfs start               # Pour verifier

 

Starting NFS services:                                     [  OK  ]

Starting NFS daemon:                                       [  OK  ]

Starting NFS mountd:                                       [  OK  ]

 

[root@kite ~]#  service nfs stop                # Car kite est en secours (a lancer si besoin)

 

Shutting down NFS daemon:                                  [  OK  ]

Shutting down NFS mountd:                                  [  OK  ]

 

 

#===================

# 19- DOKUWIKI

#===================

 

  useradd -u 3090 -g 3001 -d /web/dokuwiki  -m -s /usr/bin/ksh dokuwiki

 

  cd /web/RPM_RH64_FROM_DVD_PLATON/Packages

  yum grouplist | grep -i php

       Cela repond  "PHP Support"

 

  yum groupinstall "PHP Support"

       Install      13 Package(s)

       Total download size: 6.0 M

       Installed size: 21 M

 

      #----- Pour une install initiale -----------------------

      ## cd /web/dokuwiki

      ##     unzip   dokuwiki-2013-05-10a.tgz

      ##     tar xvf dokuwiki-2013-05-10a.tar

      ##

      #----- Pour une restauration d'un wiki deja existant ---

      #

      #   nohup scp -rp surf:/web/*wiki* /web  &

      #   (voir plus bas : Restauration depuis SURF)

      #-------------------------------------------------------

 

  vi /etc/httpd/conf/httpd.conf 

               DocumentRoot "/web/dokuwiki"

 

  chown -R apache:apache /web/dokuwiki

 

  service httpd start
  service httpd stop           # Car c'est celui de surf qui sera actif

 

 

#  Pour initialiser le site Dokuwiki :

#       - sur PC, dans Firefox :  http://10.179.0.200/index.php

#

#  Apres cette initialisation, l'acces au site se fait par :

#       http://10.179.0.200/doku.php

 

#==========================================================

# 20- AJOUT DE POSTFIX     (Fait en octobre 2014 sur SURF en RH54) 
#     pour que dokuwiki envoie des mails

#==========================================================

 

Ajout de postfix pour l'envoi des mail par le dokuwiki :

 

        # 1227678 Oct 24 15:42 cyrus-sasl-2.1.22-5.el5.x86_64.rpm   # Attention ici Rpms RH54

        # 3871806 Oct 24 15:41 postfix-2.3.3-2.1.el5_2.x86_64. rpm   # Attention ici Rpms RH54

 

rpm -ivh cyrus-sasl-2.1.22-5.el5.x86_64.rpm

rpm -ivh postfix-2.3.3-2.1.el5_2.x86_64.rpm

 

vi /etc/postfix/main.cd

 

       # Ajoute par Albert le 24-10-2014 :

       relayhost = [l4-aubemott188.bagnolet.francetelecom.fr]

       # Fin du Ajoute par Albert

 

service postfix start

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

_________________________________________________________________________________________________________________________

Ce message et ses pieces jointes peuvent contenir des informations confidentielles ou privilegiees et ne doivent donc
pas etre diffuses, exploites ou copies sans autorisation. Si vous avez recu ce message par erreur, veuillez le signaler
a l'expediteur et le detruire ainsi que les pieces jointes. Les messages electroniques etant susceptibles d'alteration,
Orange decline toute responsabilite si ce message a ete altere, deforme ou falsifie. Merci.

This message and its attachments may contain confidential or privileged information that may be protected by law;
they should not be distributed, used or copied without authorisation.
If you have received this email in error, please notify the sender and delete this message and its attachments.
As emails may be altered, Orange is not liable for messages that have been modified, changed or falsified.
Thank you.
