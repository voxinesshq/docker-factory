#cd /usr/src
#wget https://github.com/diegonehab/luasocket/archive/v3.0-rc1.zip
#unzip v3.0-rc1.zip
cd luasocket-3.0-rc1
cd src
perl -p -i.bak1 -e "s#luaL_openlib\(L, NULL, func, 0\)#luaL_register\(L, NULL, func\)#" *.c
perl -p -i.bak2 -e "s#luaL_openlib\(L, \"socket\", func, 0\)#luaL_register\(L, \"socket\", func\)#" *.c
perl -p -i.bak3 -e "s#luaL_openlib\(L, \"mime\", func, 0\)#luaL_register\(L, \"mime\", func\)#" *.c
cd ..
cd gem
perl -p -i.bak1 -e "s#luaL_openlib\(L, \"gem\", func, 0\)#luaL_register\(L, \"gem\", func\)#" *.c
cd ..
make PLAT=linux DEBUG=NODEBUG LUAV=5.2 prefix=/usr/local COMPAT=NOCOMPAT LUAINC_linux=/usr/src/freeswitch/src/mod/languages/mod_lua/lua
make PLAT=linux DEBUG=NODEBUG LUAV=5.2 prefix=/usr/local COMPAT=NOCOMPAT LUAINC_linux=/usr/src/freeswitch/src/mod/languages/mod_lua/lua install