http = require "socket.http";
 
response_body, response_status_code, response_headers, response_status_line = http.request("http://www.example.com/");
 
stream:write("response_status_code = " .. response_status_code .. "\n");
stream:write("response_body = " .. response_body .. "\n");
 
for key,value in pairs(response_headers)
do
stream:write("response_headers = " .. key .. " => " .. value .. "\n") ;
end;
 
stream:write("response_status_line = " .. response_status_line .. "\n");